package com.tr.setek.roboback;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = "com.tr.setek")
public class RobobackApplication {

	public static void main(String[] args) {
		SpringApplication.run(RobobackApplication.class, args);
	}

}

