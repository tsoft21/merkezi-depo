package com.tr.setek.roboback.service;

import com.tr.setek.roboback.constant.ResultCode;
import com.tr.setek.roboback.dataaccess.ekol.OperationDataServiceClient;
import com.tr.setek.roboback.dto.BaseResponse;
import ekol.wsdl.operationdataservice.GetReturnDataResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * EKOL operation data service related business logic
 *
 * @author Yusuf Ismail Oktay
 * @since 0.0.1
 */
@Service public class OperationDataService {

    @Autowired private OperationDataServiceClient operationDataServiceClient;

    private Logger log = LoggerFactory.getLogger(OperationDataService.class);

    public BaseResponse getReturnData() {
        log.debug("Get return data");
        GetReturnDataResponse returnData = operationDataServiceClient.getReturnData();//TODO DTO
        return new BaseResponse(ResultCode.SUCCESS);
    }
}
