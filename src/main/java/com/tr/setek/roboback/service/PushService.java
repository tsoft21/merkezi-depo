package com.tr.setek.roboback.service;

import com.media_saturn.esb.bdm.transferorderv2management.v1.Response;
import com.media_saturn.esb.bdm.transferorderv2management.v1.TransferType;
import com.tr.setek.roboback.constant.ResultCode;
import com.tr.setek.roboback.dataaccess.mediasaturn.soliddb.persistence.mediasaturn.push.PushServiceClient;
import com.tr.setek.roboback.dto.ArticleDto;
import com.tr.setek.roboback.dto.BaseResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * EKOL operation data service related business logic
 *
 * @author Yusuf Ismail Oktay
 * @since 0.0.1
 */
@Service public class PushService {

    @Autowired private PushServiceClient pushServiceClient;

    private Logger log = LoggerFactory.getLogger(PushService.class);

    public BaseResponse sendArticle(List<ArticleDto> articles) {
        log.debug("send article");
        for (ArticleDto dto:articles){
            TransferType request=new TransferType();

            Response resp = pushServiceClient.sendArticle(request);//TODO DTO
        }


        return new BaseResponse(ResultCode.SUCCESS);
    }
}
