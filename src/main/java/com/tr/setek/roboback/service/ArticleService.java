package com.tr.setek.roboback.service;

import com.tr.setek.roboback.dto.ArticleDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Transactional
@Service
public class ArticleService {

    @Autowired
    private ExcelService excelService;


    @Autowired
    private RobobackService robobackService;

    @Autowired
    private PushService pushService;

    @Scheduled(fixedRate = 6000000)
    public void syncronizeArticles(){
        List<ArticleDto> data = excelService.readExcel();
        robobackService.saveArticleData(data);
        pushService.sendArticle(data);

    }

}
