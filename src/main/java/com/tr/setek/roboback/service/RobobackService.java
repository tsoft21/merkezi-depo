package com.tr.setek.roboback.service;

import com.tr.setek.roboback.dataaccess.roboback.model.Dummy;
import com.tr.setek.roboback.dataaccess.roboback.persistence.DummyRepository;
import com.tr.setek.roboback.dto.ArticleDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Iterator;
import java.util.List;

@Service
public class RobobackService {

    @Autowired
    private DummyRepository dummyRepository;

    public String addNewDummy (String name ) {
        Dummy newDummy = new Dummy ();
        newDummy .setName(name);
        dummyRepository.save(newDummy);
        return "Saved";
    }

    public void saveArticleData(List<ArticleDto> data) {
        //TODO article repository
        dummyRepository.saveAll(/*data*/ new Iterable<Dummy>() {
            @Override
            public Iterator<Dummy> iterator() {
                return null;
            }
        });
    }


}
