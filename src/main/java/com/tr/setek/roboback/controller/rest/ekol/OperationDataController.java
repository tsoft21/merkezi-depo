package com.tr.setek.roboback.controller.rest.ekol;

import com.tr.setek.roboback.dto.BaseResponse;
import com.tr.setek.roboback.service.OperationDataService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


/**
 *
 * TODO Data , service ve controllerlara arasinda DTO kullanilmali. Birebir model bagimliligi olmamali
 *
 * @author Yusuf Ismail Oktay
 * @since 0.0.1
 */
@RestController()
@RequestMapping("/v1/ekol/operationdata")
class OperationDataController {

    @Autowired OperationDataService operationDataService;

    private Logger log = LoggerFactory.getLogger(OperationDataController.class);


    @GetMapping("/returnData")
    @ResponseBody
    public BaseResponse getReturnData() {
        log.debug("get return data service triggered");
        return operationDataService.getReturnData();
    }

    /**
     * Returns article stock information
     *
     * @param articleStockCode Unique article stock code
     * @return Article stock information
     */
    @GetMapping("/article/{articleStockCode}")
    @ResponseBody
    public BaseResponse getArticleStock(@PathVariable Long articleStockCode) {
        log.debug("Get Article Stock of articleStockCode {}", articleStockCode);
        return null;
    }

}
