package com.tr.setek.roboback.constant;

public class ErrorMessage {

    public static final String ID_IS_MISSING = "ID is missing";
    public static final String NO_DATA_FOUND = "No data can be found";

}
