package com.tr.setek.roboback.constant;

public enum ResultCode {
    SUCCESS("Success"),
    ERROR("An error occured!"),
    SAVED_SUCCESSFULLY("Saved Successfully");

    private final String desc;

    ResultCode(String desc) {
        this.desc = desc;
    }

    public String getDesc() {
        return desc;
    }
}
