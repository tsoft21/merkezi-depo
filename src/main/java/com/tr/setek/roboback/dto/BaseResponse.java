package com.tr.setek.roboback.dto;

import com.tr.setek.roboback.constant.ResultCode;

public class BaseResponse {
    private ResultCode result;
    private String desc;

    public BaseResponse() {
    }

    public BaseResponse(ResultCode result) {
        this.setResult(result);
        this.setDesc(result.getDesc());
    }

    public ResultCode getResult() {
        return result;
    }

    public void setResult(ResultCode result) {
        this.result = result;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

}
