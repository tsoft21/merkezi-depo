package com.tr.setek.roboback.dataaccess.ekol;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;

/**
 * EKOL OperationDataService Config
 *
 *
 * @author Yusuf Ismail Oktay
 */
@Configuration
public class OperationDataServiceConfig  {

    private Logger log = LoggerFactory.getLogger(OperationDataServiceConfig.class);

    @Bean
    public Jaxb2Marshaller marshaller() {
        Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
        // this package must match the package in the <generatePackage> specified in
        // pom.xml
        marshaller.setContextPath("ekol.wsdl.operationdataservice");
        return marshaller;
    }

    @Bean
    public OperationDataServiceClient operationDataServiceClient(Jaxb2Marshaller marshaller) {
        OperationDataServiceClient client = new OperationDataServiceClient();
        client.setDefaultUri("http://web01.ekol.com/WarehouseService-Test/OperationDataService.svc");
        client.setMarshaller(marshaller);
        client.setUnmarshaller(marshaller);
        return client;
    }
}
