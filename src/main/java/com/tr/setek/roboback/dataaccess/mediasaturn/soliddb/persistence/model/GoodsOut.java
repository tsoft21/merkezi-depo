package com.tr.setek.roboback.dataaccess.mediasaturn.soliddb.persistence.model;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;

@Entity
public class GoodsOut extends BaseEntity {
	
	@Column(name = "WORK_ORDER_NR")
	String workOrderNr;

	@Column(name = "EXPECTED_DATE_ISSUE")
	Date expectedDateIssue;

	@Column(name = "EXPECTED_DATE_DELIVERY")
	Date expectedDateDelivery;

	@Column(name = "SHIPMENT_COMPANY_CODE")
	String shipmentCompanyCode;

	@Column(name = "FROM_STOCK_LOCATION")
	String fromStockLocation;
	
	public GoodsOut() {
		
	}

	public GoodsOut(String workOrderNr, Date expectedDateIssue, Date expectedDateDelivery, String shipmentCompanyCode, String fromStockLocation) {
		this.workOrderNr = workOrderNr;
		this.expectedDateIssue = expectedDateIssue;
		this.expectedDateDelivery = expectedDateDelivery;
		this.shipmentCompanyCode = shipmentCompanyCode;
		this.fromStockLocation = fromStockLocation;
	}

	public String getWorkOrderNr() {
		return workOrderNr;
	}

	public void setWorkOrderNr(String workOrderNr) {
		this.workOrderNr = workOrderNr;
	}

	public Date getExpectedDateIssue() {
		return expectedDateIssue;
	}

	public void setExpectedDateIssue(Date expectedDateIssue) {
		this.expectedDateIssue = expectedDateIssue;
	}

	public Date getExpectedDateDelivery() {
		return expectedDateDelivery;
	}

	public void setExpectedDateDelivery(Date expectedDateDelivery) {
		this.expectedDateDelivery = expectedDateDelivery;
	}

	public String getShipmentCompanyCode() {
		return shipmentCompanyCode;
	}

	public void setShipmentCompanyCode(String shipmentCompanyCode) {
		this.shipmentCompanyCode = shipmentCompanyCode;
	}

	public String getFromStockLocation() {
		return fromStockLocation;
	}

	public void setFromStockLocation(String fromStockLocation) {
		this.fromStockLocation = fromStockLocation;
	}
	
}
