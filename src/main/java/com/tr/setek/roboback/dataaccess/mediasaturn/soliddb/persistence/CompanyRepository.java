package com.tr.setek.roboback.dataaccess.mediasaturn.soliddb.persistence;

import com.tr.setek.roboback.dataaccess.mediasaturn.soliddb.persistence.model.Company;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CompanyRepository extends JpaRepository<Company, Long> {

	
	
}
