package com.tr.setek.roboback.dataaccess.mediasaturn.soliddb.persistence.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
public class BaseEntity implements Serializable  {


	@Id
    @Column(name="ID")
    @GeneratedValue(strategy= GenerationType.AUTO)
	protected Long id;

	public BaseEntity() {
		
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
}
