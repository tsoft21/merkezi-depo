package com.tr.setek.roboback.dataaccess.mediasaturn.soliddb.persistence;

import com.tr.setek.roboback.dataaccess.mediasaturn.soliddb.persistence.model.Article;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ArticleRepository extends JpaRepository<Article, Long> {

	
	
}
