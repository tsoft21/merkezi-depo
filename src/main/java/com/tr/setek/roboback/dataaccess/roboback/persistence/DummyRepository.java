package com.tr.setek.roboback.dataaccess.roboback.persistence;

import com.tr.setek.roboback.dataaccess.roboback.model.Dummy;
import org.springframework.data.repository.CrudRepository;

public interface DummyRepository extends CrudRepository<Dummy, Integer> {

}