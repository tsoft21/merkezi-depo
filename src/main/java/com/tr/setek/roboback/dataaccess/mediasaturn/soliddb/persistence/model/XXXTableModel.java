package com.tr.setek.roboback.dataaccess.mediasaturn.soliddb.persistence.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity public class XXXTableModel {


    @Id @Column(nullable = false) private Long id;

    @Column private byte[] left;
    @Column private byte[] right;

    public XXXTableModel() {

    }

    public XXXTableModel(long id) {
        this.id = id;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public byte[] getLeft() {
        return left;
    }

    public void setLeft(byte[] left) {
        this.left = left;
    }

    public byte[] getRight() {
        return right;
    }

    public void setRight(byte[] right) {
        this.right = right;
    }
}
