package com.tr.setek.roboback.dataaccess.ekol;

import com.tr.setek.roboback.constant.EkolCommons;
import ekol.wsdl.operationdataservice.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;
import org.springframework.ws.soap.client.core.SoapActionCallback;

/**
 * EKOL OperationDataService Client
 * <p>
 * TODO marshaller configurationgit add
 * <p>
 * TODO transactionCode generator
 * <p>
 * TODO Endpoint adresi test/prod ortamina gore dinamik degismeli. Ortam bilgisi sunucuda harici olarak bir degiskende olmali
 *
 * @author Yusuf Ismail Oktay
 */
public class OperationDataServiceClient extends WebServiceGatewaySupport {

    private Logger log = LoggerFactory.getLogger(OperationDataServiceClient.class);

    private ObjectFactory factory = new ObjectFactory();

    public GetReturnDataResponse getReturnData() {

        GetReturnData request = new GetReturnData();
        request.setApiKey(factory.createGetReturnDataApiKey(EkolCommons.API_KEY));
        request.setTransactionCode(factory.createGetReturnDataTransactionCode("12345678"));//TODO

        return (GetReturnDataResponse) getWebServiceTemplate().marshalSendAndReceive(request,
                new SoapActionCallback("http://schemas.ekol.com/clientdataexch/2011/IOperationDataService/GetReturnData"));

    }

    public GetArticleStockResponse getArticleStock(String articleCode) {

        log.debug("Requesting article stock for {}", articleCode);

        GetArticleStock request = new GetArticleStock();
        request.setArticleCode(factory.createGetArticleStockArticleCode(articleCode));
        request.setApiKey(factory.createGetArticleStockApiKey(EkolCommons.API_KEY));

        return (GetArticleStockResponse) getWebServiceTemplate().marshalSendAndReceive(request);
    }


}
