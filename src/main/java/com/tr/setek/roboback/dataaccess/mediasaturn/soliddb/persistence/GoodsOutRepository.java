package com.tr.setek.roboback.dataaccess.mediasaturn.soliddb.persistence;

import com.tr.setek.roboback.dataaccess.mediasaturn.soliddb.persistence.model.GoodsOut;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GoodsOutRepository extends JpaRepository<GoodsOut, Long> {

	
	
}
