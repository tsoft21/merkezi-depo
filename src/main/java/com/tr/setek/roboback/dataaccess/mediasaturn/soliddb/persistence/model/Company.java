package com.tr.setek.roboback.dataaccess.mediasaturn.soliddb.persistence.model;

import javax.persistence.Column;
import javax.persistence.Entity;

@Entity
public class Company extends BaseEntity {
	
	@Column(name = "COMPANY_CODE")
	private String companyCode;

	@Column(name = "COMPANY_NAME_1")
	private String companyName1;

	@Column(name = "ADRESS_1")
	private String adress1;

	@Column(name = "CITY")
	private String city;

	@Column(name = "COUNTRY_CODE")
	private String countryCode;

	@Column(name = "COUNTRY_NAME")
	private String countryName;

	@Column(name = "EMAIL")
	private String email;

	@Column(name = "TAX_OFFICE")
	private String taxOffice;

	@Column(name = "TAX_NUMBER")
	private long taxNumber;

	@Column(name = "VC_FLAG")
	private int vcFlag;

	@Column(name = "VC_FLAG_DESCRIPTION")
	private String vcFlagDescription;

	@Column(name = "TELEPHONE_1")
	private long telephone1;

	@Column(name = "PROJECT_ID")
	private int projectId;

	public Company() {
		
	}

	public Company(String companyCode, String companyName1, String adress1, String city, String countryCode,
			String countryName, String email, String taxOffice, long taxNumber, int vcFlag, String vcFlagDescription,
			long telephone1, int projectId) {
		
		this.companyCode = companyCode;
		this.companyName1 = companyName1;
		this.adress1 = adress1;
		this.city = city;
		this.countryCode = countryCode;
		this.countryName = countryName;
		this.email = email;
		this.taxOffice = taxOffice;
		this.taxNumber = taxNumber;
		this.vcFlag = vcFlag;
		this.vcFlagDescription = vcFlagDescription;
		this.telephone1 = telephone1;
		this.projectId = projectId;
	}

	public String getCompanyCode() {
		return companyCode;
	}

	public void setCompanyCode(String companyCode) {
		this.companyCode = companyCode;
	}

	public String getCompanyName1() {
		return companyName1;
	}

	public void setCompanyName1(String companyName1) {
		this.companyName1 = companyName1;
	}

	public String getAdress1() {
		return adress1;
	}

	public void setAdress1(String adress1) {
		this.adress1 = adress1;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public String getCountryName() {
		return countryName;
	}

	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTaxOffice() {
		return taxOffice;
	}

	public void setTaxOffice(String taxOffice) {
		this.taxOffice = taxOffice;
	}

	public long getTaxNumber() {
		return taxNumber;
	}

	public void setTaxNumber(long taxNumber) {
		this.taxNumber = taxNumber;
	}

	public int getVcFlag() {
		return vcFlag;
	}

	public void setVcFlag(int vcFlag) {
		this.vcFlag = vcFlag;
	}

	public String getVcFlagDescription() {
		return vcFlagDescription;
	}

	public void setVcFlagDescription(String vcFlagDescription) {
		this.vcFlagDescription = vcFlagDescription;
	}

	public long getTelephone1() {
		return telephone1;
	}

	public void setTelephone1(long telephone1) {
		this.telephone1 = telephone1;
	}

	public int getProjectId() {
		return projectId;
	}

	public void setProjectId(int projectId) {
		this.projectId = projectId;
	}
}
