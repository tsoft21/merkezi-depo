package com.tr.setek.roboback.dataaccess.mediasaturn.soliddb.persistence;

import com.tr.setek.roboback.dataaccess.mediasaturn.soliddb.persistence.model.GoodsIn;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GoodsInRepository extends JpaRepository<GoodsIn, Long> {

	
	
}
