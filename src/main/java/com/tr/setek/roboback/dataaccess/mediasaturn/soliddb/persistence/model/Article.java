package com.tr.setek.roboback.dataaccess.mediasaturn.soliddb.persistence.model;

import javax.persistence.Column;
import javax.persistence.Entity;

@Entity
public class Article extends BaseEntity {

		@Column(name="ARTICLE_CODE")
		private String articleCode;
		
		@Column(name="DESCRIPTION")
		private String description;
		
		@Column(name="UOM_PURCHASING")
		private String uomPurchasing;
		
		@Column(name="UOM_PURCHASING_DESCRIPTION")
		private String uomPurchasingDescription;
		
		@Column(name="COLOR_CODE")
		private String colorCode;
		
		@Column(name="COLOR_DESCRIPTION")
		private String colorDescription;
		
		@Column(name="BARCODE")
		private String barcode;
		
		@Column(name="LOCAL_PRODUCT_CODE")
		private String localProductCode;
		
		public Article() {
			
		}

		public Article(String articleCode, String description, String uomPurchasing, String uomPurchasingDescription,
				String colorCode, String colorDescription, String barcode, String localProductCode) {
			
			this.articleCode = articleCode;
			this.description = description;
			this.uomPurchasing = uomPurchasing;
			this.uomPurchasingDescription = uomPurchasingDescription;
			this.colorCode = colorCode;
			this.colorDescription = colorDescription;
			this.barcode = barcode;
			this.localProductCode = localProductCode;
		}

		public String getArticleCode() {
			return articleCode;
		}

		public void setArticleCode(String articleCode) {
			this.articleCode = articleCode;
		}

		public String getDescription() {
			return description;
		}

		public void setDescription(String description) {
			this.description = description;
		}

		public String getUomPurchasing() {
			return uomPurchasing;
		}

		public void setUomPurchasing(String uomPurchasing) {
			this.uomPurchasing = uomPurchasing;
		}

		public String getUomPurchasingDescription() {
			return uomPurchasingDescription;
		}

		public void setUomPurchasingDescription(String uomPurchasingDescription) {
			this.uomPurchasingDescription = uomPurchasingDescription;
		}

		public String getColorCode() {
			return colorCode;
		}

		public void setColorCode(String colorCode) {
			this.colorCode = colorCode;
		}

		public String getColorDescription() {
			return colorDescription;
		}

		public void setColorDescription(String colorDescription) {
			this.colorDescription = colorDescription;
		}

		public String getBarcode() {
			return barcode;
		}

		public void setBarcode(String barcode) {
			this.barcode = barcode;
		}

		public String getLocalProductCode() {
			return localProductCode;
		}

		public void setLocalProductCode(String localProductCode) {
			this.localProductCode = localProductCode;
		}
		
}
