package com.tr.setek.roboback.dataaccess.mediasaturn.soliddb.persistence.mediasaturn.push;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;

/**
 * Push  Servie config Config
 *
 *TODO
 * @author Yusuf Ismail Oktay
 */
@Configuration
public class PushServiceConfig {

    private Logger log = LoggerFactory.getLogger(PushServiceConfig.class);

    @Bean
    public Jaxb2Marshaller marshaller() {
        Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
        // this package must match the package in the <generatePackage> specified in
        // pom.xml
        marshaller.setContextPath("com.media_saturn.esb.bdm.transferorderv2management.v1");
        return marshaller;
    }

    @Bean
    public PushServiceClient operationDataServiceClient(Jaxb2Marshaller marshaller) {
        PushServiceClient client = new PushServiceClient();
        client.setDefaultUri("TODO http://web01.ekol.com/WarehouseService-Test/OperationDataService.svc");
        client.setMarshaller(marshaller);
        client.setUnmarshaller(marshaller);
        return client;
    }
}
