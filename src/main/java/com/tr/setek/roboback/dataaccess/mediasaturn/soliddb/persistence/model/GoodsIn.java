package com.tr.setek.roboback.dataaccess.mediasaturn.soliddb.persistence.model;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;

@Entity
public class GoodsIn extends BaseEntity {
	
	@Column(name = "ASN_NR")
	private String asnNr;

	@Column(name = "COMPANY_CODE")
	private String companyCode;

	@Column(name = "ORDER_TYPE")
	private int orderType;
	
	@Column(name = "EXPECTED_DATE")
	private Date expectedDate;

	@Column(name = "PROJECT_ID")
	private int projectId;

	@Column(name = "BOL_DATE")
	private Date bolDate;
	
	public GoodsIn() {
		
	}

	public GoodsIn(String asnNr, String companyCode, int orderType, Date expectedDate, int projectId, Date bolDate) {
		this.asnNr = asnNr;
		this.companyCode = companyCode;
		this.orderType = orderType;
		this.expectedDate = expectedDate;
		this.projectId = projectId;
		this.bolDate = bolDate;
	}

	public String getAsnNr() {
		return asnNr;
	}

	public void setAsnNr(String asnNr) {
		this.asnNr = asnNr;
	}

	public String getCompanyCode() {
		return companyCode;
	}

	public void setCompanyCode(String companyCode) {
		this.companyCode = companyCode;
	}

	public int getOrderType() {
		return orderType;
	}

	public void setOrderType(int orderType) {
		this.orderType = orderType;
	}

	public Date getExpectedDate() {
		return expectedDate;
	}

	public void setExpectedDate(Date expectedDate) {
		this.expectedDate = expectedDate;
	}

	public int getProjectId() {
		return projectId;
	}

	public void setProjectId(int projectId) {
		this.projectId = projectId;
	}

	public Date getBolDate() {
		return bolDate;
	}

	public void setBolDate(Date bolDate) {
		this.bolDate = bolDate;
	}

}
