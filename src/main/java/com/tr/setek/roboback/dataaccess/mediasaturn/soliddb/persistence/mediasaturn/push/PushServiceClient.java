package com.tr.setek.roboback.dataaccess.mediasaturn.soliddb.persistence.mediasaturn.push;

import com.media_saturn.esb.bdm.transferorderv2management.v1.ObjectFactory;
import com.media_saturn.esb.bdm.transferorderv2management.v1.Response;
import com.media_saturn.esb.bdm.transferorderv2management.v1.TransferType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;
import org.springframework.ws.soap.client.core.SoapActionCallback;

import javax.xml.bind.JAXBElement;

/**
 * EKOL OperationDataService Client
 * <p>
 * TODO marshaller configurationgit add
 * <p>
 * TODO transactionCode generator
 * <p>
 * TODO Endpoint adresi test/prod ortamina gore dinamik degismeli. Ortam bilgisi sunucuda harici olarak bir degiskende olmali
 *
 * @author Yusuf Ismail Oktay
 */
public class PushServiceClient extends WebServiceGatewaySupport {

    private Logger log = LoggerFactory.getLogger(PushServiceClient.class);

    private ObjectFactory factory = new ObjectFactory();

    public Response sendArticle(TransferType request) {
        log.debug("send article {}");

        request.setSenderMessageId("dummy sender message id");
        request.setToOutletId(12356666);
        JAXBElement<TransferType> soapRequest = factory.createRequest(request);

        return (Response) getWebServiceTemplate().marshalSendAndReceive(soapRequest ,
                new SoapActionCallback("TODO URL duzenlenmeli http://schemas.ekol.com/clientdataexch/2011/IOperationDataService/GetReturnData"));

    }


}
